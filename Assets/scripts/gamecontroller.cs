﻿using UnityEngine;
using System.Collections;

public class gamecontroller : MonoBehaviour {

	public static gamecontroller Instance;

	private boardcontroller boardController;

	void Awake () {
		if (Instance != null && Instance != this) {
			Destroy(gameObject);
			return;
		}
		Instance = this;
		DontDestroyOnLoad (gameObject);
		boardController = GetComponent<boardcontroller>();
	}

	void Start () {
		boardController.SetupLevel();
	}

	void Update () {
	
	}

}
